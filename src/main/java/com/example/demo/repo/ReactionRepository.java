package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Reaction;

@Repository
public interface ReactionRepository extends JpaRepository<Reaction, String>{

}
