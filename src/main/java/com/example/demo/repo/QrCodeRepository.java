package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.model.QrCode;

@RepositoryRestResource(collectionResourceRel = "qrcodes", path = "qrcodes")
public interface QrCodeRepository extends JpaRepository<QrCode, Long> {

}
