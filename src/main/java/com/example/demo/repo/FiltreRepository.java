package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.model.Filtre;

@RepositoryRestResource(collectionResourceRel = "filtres", path = "filtres")
public interface FiltreRepository extends JpaRepository<Filtre, Long>{

}
