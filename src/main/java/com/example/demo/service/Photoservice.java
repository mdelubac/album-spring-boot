package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Photo;

public interface Photoservice {
	public void uploadToDb(MultipartFile photo);
	public Optional<Photo> getPhotoById(String id);
	public List<Photo> getAllPhotos();
	public void deletePhotoById(String id);
	
	

}
