
package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Role;


public interface RoleService {
	
	public Role saveOrUpdateRole (Role role);
	public void deleteRole(Long id);
	public List<Role> findAllRole();
	public Optional<Role> findByid(Long id);

}
