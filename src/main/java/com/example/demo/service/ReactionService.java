package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Reaction;


public interface ReactionService {
	
	public void uploadToDb(MultipartFile file);
	public Optional<Reaction> getReactionById(String reactionId);
	public void deleteMyReaction(String reactionId);
	public List<Reaction> findAllReaction();
}
