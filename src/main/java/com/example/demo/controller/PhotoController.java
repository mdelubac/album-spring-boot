package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Photo;
import com.example.demo.service.Photoservice;

@RestController
@RequestMapping
public class PhotoController {
	@Autowired
	private Photoservice photoService;
	
	@PostMapping("/save/photo")
	public void saveOrUpdateMyPhoto(@RequestParam("photo") MultipartFile photo) {
		photoService.uploadToDb(photo);
	}
	
	@GetMapping("/getphoto/{id}")
	public Optional<Photo> getPhotoMyById(@PathVariable String id) {
		return photoService.getPhotoById(id);
		
	}
	
	@GetMapping("/getAllPhotos")
	public List<Photo> getAllMyPhotos(){
		return photoService.getAllPhotos();
	}
	
	@DeleteMapping("/deletePhoto/{id}")
	public void deleteMyPhoto(@PathVariable String id) {
		photoService.deletePhotoById(id);
	}

}
