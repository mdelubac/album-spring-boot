package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Reaction;
import com.example.demo.service.ReactionService;

@RestController
@RequestMapping
public class ReactionController {

	@Autowired
	ReactionService reactionService;

	@PostMapping("/save/reaction")
	public void saveOrUpdateMyReaction(@RequestParam("reaction") MultipartFile multipartFile) {
		reactionService.uploadToDb(multipartFile);
	}

	@GetMapping("/getIcon/{reactionId}")
	public Optional<Reaction> getReactionById(@PathVariable String reactionId) {
		return reactionService.getReactionById(reactionId);
	}

	@GetMapping("/getAllReaction")
	public List<Reaction> getAllMyReaction() {
		return reactionService.findAllReaction();
	}

	@DeleteMapping("/deleteReaction/{reactionId}")
	public void deleteMyReaction(@PathVariable String reactionId) {
		reactionService.deleteMyReaction(reactionId);
	}

}
