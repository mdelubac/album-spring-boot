
package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Role;
import com.example.demo.service.RoleService;

@CrossOrigin
@RestController
@RequestMapping
public class RoleController {
	
	@Autowired RoleService roleSer;
	
	@PostMapping("/saveRole")
	public Role saveRole (@RequestBody Role role) {
		return roleSer.saveOrUpdateRole(role);
	}
	
	@PutMapping("/upDateRole")
	public Role upDateRole (@RequestBody Role role) {
		return roleSer.saveOrUpdateRole(role);
	}
	
	@GetMapping("/findRoleById/{id}")
	public Optional<Role> findRoleById(@PathVariable("id") Long id) {
		return roleSer.findByid(id);
	}
	@DeleteMapping("/deleteRoleById/{id}")
	public void deleteARole(@PathVariable("id") Long id) {
		roleSer.deleteRole(id);
	}
	
	@GetMapping("/getAllRoles")
	public List<Role> getAllMyRoles(){
		return roleSer.findAllRole();
	}

}
