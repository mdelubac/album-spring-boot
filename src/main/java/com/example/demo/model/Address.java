package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Address {
	
	@Column(nullable = true)
	private int streetNumber;
	@Column(nullable = true)
	private String streetName;
	@Column(nullable = true)
	private String city;
	@Column(nullable = true)
	private String postalCode;
	@Column(nullable = true)
	private String country;
	@Column(nullable = true)
	private double longitude;
	@Column(nullable = true)
	private double lattitude;
}
