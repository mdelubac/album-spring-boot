package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUser;
	private String userName;
	private String password;
	private String email;
	private String biography;
	
	
	@Embedded
	private Address address;
	
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "USER_CREATED_EVENT",
	joinColumns = @JoinColumn(name="FK_USER", referencedColumnName = "idUser"),
	inverseJoinColumns = @JoinColumn(name="FK_CREATED_EVENT",referencedColumnName = "idEvent"))
	private List<Event> eventListCreated;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "USER_GUEST_EVENT",
	joinColumns = @JoinColumn(name="FK_USER", referencedColumnName = "idUser"),
	inverseJoinColumns = @JoinColumn(name="FK_GUEST_EVENT",referencedColumnName = "idEvent"))
	private List<Event> eventListAsGuest;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "USER_ROLE",
	joinColumns = @JoinColumn(name="FK_USER", referencedColumnName = "idUser"),
	inverseJoinColumns = @JoinColumn(name="FK_ROLE", referencedColumnName = "id"))
	private List<Role> roles;
	
	@Embedded
	private Official officialUser;
	
	/**TODO
	 * nearGuest
	 */
	
	
	

	

}
