package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Photo {
	
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long idPhoto;
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	private String photoId;
	private String photoName;
	private String photoType;
	@Lob
	private byte[] photoData;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_PHOTO", referencedColumnName = "photoId")
	private List<Message> comments;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_PHOTO", referencedColumnName = "photoId")
	private List<Reaction> reactions; 
	
	/* Quand on utilisait en Primary key Long idPhoto
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_PHOTO", referencedColumnName = "idPhoto")
	private List<Message> comments;
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_PHOTO", referencedColumnName = "idPhoto")
	private List<Reaction> reactions; 
	
	
	Peut être nous servira plus tard =
	  
	@OneToOne
	@JoinColumn(name="FK_FILTRE", referencedColumnName = "idFiltre")
	private Filtre filtre;
 */
}
