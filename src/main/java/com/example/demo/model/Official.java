package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Official {
		
	
	@Column(nullable = true)
	private String oname;
	@Column(nullable = true)
	private byte[] logo;
	@Column(nullable = true)
	private boolean valid;
	
}
