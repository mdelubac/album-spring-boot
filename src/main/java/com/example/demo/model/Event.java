package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Event {
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEvent;
	private String description;
	private String eventName;
	private String hyperLink;
	private String type;
	
//	private Photo backgroundImage;
	// private Date date chercher le bon format pour beginDate et endDate
	
//	private List<User> guestList;
	
	//private List<User> nearGuest;
	
//	private List<User> organizer;
	
	@Embedded
	private Address address;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_EVENT", referencedColumnName = "idEvent")
	private List<Album> albums;
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_EVENT", referencedColumnName = "idEvent")
	private List<Message> messages;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FK_EVENT", referencedColumnName = "idEvent")

	private List<Reaction> reactions; 
	
	
//	private QrCode qrcode;
	@Embedded
	private Official officialEvent;
	
}
