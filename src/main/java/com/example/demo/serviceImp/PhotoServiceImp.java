package com.example.demo.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Photo;
import com.example.demo.repo.PhotoRepository;
import com.example.demo.service.Photoservice;

@Service
public class PhotoServiceImp implements Photoservice {
	
	@Autowired
	private PhotoRepository photoRepository;

	@Override
	public void uploadToDb(MultipartFile photo) {
		// TODO Auto-generated method stub
		Photo maPhoto = new Photo();
		try {
			maPhoto.setPhotoData(photo.getBytes());
			maPhoto.setPhotoType(photo.getContentType());
			maPhoto.setPhotoName(photo.getOriginalFilename());
			photoRepository.save(maPhoto);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	@Override
	public Optional<Photo> getPhotoById(String id) {
		// TODO Auto-generated method stub
		return photoRepository.findById(id);
	}


	@Override
	public List<Photo> getAllPhotos() {
		// TODO Auto-generated method stub
		return photoRepository.findAll();
	}

	@Override
	public void deletePhotoById(String id) {
		// TODO Auto-generated method stub
		photoRepository.deleteById(id);
		
	}



	

}
