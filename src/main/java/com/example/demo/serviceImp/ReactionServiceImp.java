package com.example.demo.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Reaction;
import com.example.demo.repo.ReactionRepository;
import com.example.demo.service.ReactionService;

@Service
public class ReactionServiceImp implements ReactionService {
	
	@Autowired
	ReactionRepository reactionRepository;

	@Override
	public void uploadToDb(MultipartFile file) {
		// TODO Auto-generated method stub
		Reaction reaction= new Reaction();
		try {
			reaction.setIconData(file.getBytes());
			reaction.setReactionType(file.getContentType());
			reaction.setReactionName(file.getName());
			reactionRepository.save(reaction);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public Optional<Reaction> getReactionById(String reactionId) {
		// TODO Auto-generated method stub
		return reactionRepository.findById(reactionId);
	}

	@Override
	public void deleteMyReaction(String iconId) {
		// TODO Auto-generated method stub
		reactionRepository.deleteById(iconId);
	}

	@Override
	public List<Reaction> findAllReaction() {
		// TODO Auto-generated method stub
		return reactionRepository.findAll();
	}
	
	
}
