package com.example.demo.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Role;
import com.example.demo.repo.RoleRepository;
import com.example.demo.service.RoleService;

@Service
public class RoleServiceImp implements RoleService {
	
	@Autowired RoleRepository roleRepo;

	@Override
	public Role saveOrUpdateRole(Role role) {
		// TODO Auto-generated method stub
		return roleRepo.save(role);
	}

	@Override
	public void deleteRole(Long id) {
		// TODO Auto-generated method stub
		roleRepo.deleteById(id);
	}

	@Override
	public List<Role> findAllRole() {
		// TODO Auto-generated method stub
		return roleRepo.findAll();
	}

	@Override
	public Optional<Role> findByid(Long id) {
		// TODO Auto-generated method stub
		return roleRepo.findById(id);
	}

}
